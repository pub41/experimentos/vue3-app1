import login from '../components/Login'
import routeValidade from './validate'
import {requestGet} from '../services/http-client/'
import filho from '../components/Componente-filho'
import filho2 from '../components/Componente-filho2'
import home from '../components/Home'



const route = [
    {
        path: '/',
        component: home,
        auth: true
    },    
    {
        path: '/login',
        component: login,
        auth: false
    },
    {
        path: '/logout',
        component: () => {
            localStorage.clear()
            window.location.href = '/'
        },
        auth: false
    },    
    {
        path: '/componentefilho',
        component: filho,
        auth: true
    },
    {
        path: '/componentefilho2',
        component: filho2,
        auth: false
    }
]

export const vueRoute = routeValidade(route, requestGet)
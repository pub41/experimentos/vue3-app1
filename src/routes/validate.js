const routeValidade = (route, requestGet) => {
    let currentRoute = window.location.pathname
    let routeNotFound = () => `Rota [${currentRoute}] não definida`
    let routeRequest
    
    [routeRequest] = route.filter(item => currentRoute === item.path)
    
    if (!routeRequest) {
        return routeNotFound
    } 

    if (routeRequest.auth) {
        requestGet('/api/auth/checktoken').then(response => {
            if (response.status === 401) {
                window.location.href = '/login'
                return
            }
        })
        return routeRequest.component
    } else {
        return routeRequest.component
    }
}
export default routeValidade
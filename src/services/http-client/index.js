import axios from 'axios'

const instance = axios.create({
    baseURL: 'http://172.17.0.1:8001',
    headers: {'Authorization': `Bearer ${JSON.parse(localStorage.getItem('minha-aplicacao'))}`}
  })

const requestPost = (uri, payload) => {
    console.log('chegou na requestPost Axios')
    return instance.post(uri, payload).then(response => response).catch(error => error.response)
}

const requestGet = (uri, params) => {
    console.log('chegou na requestGet Axios')
    return instance.get(uri, params).then(response => response).catch(error => error.response)
}

export {
    requestPost,
    requestGet
}
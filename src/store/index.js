import {reactive} from 'vue'

const state = reactive({
    pessoa: {
        nome: 'Meu nome',
        email: 'email@email'
    },
    carro: {
        marca: 'Nissa',
        ano: '2011'
    }
})

const setState = (propriedade, valor) => {    
  state[propriedade] = valor
}

export {    
    state,
    setState
}